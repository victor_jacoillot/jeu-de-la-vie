package fr.jeu.vie;

public class AutomateCellulaires {

	static int generation = 0;
	static int MORTE = 0;
	static int VIVANT = 1;

	static int size = 8;
	static int[][] plateau;


	public static void AffichagePlateauJeu(int[][] tab){
		int count = 0;
		for(int i = 0; i < size; i++){
			for(int j = 0; j < size; j++){
				if(tab[i][j] == 1){
					count++;
					System.out.print(VIVANT + " ");
				}else {
					System.out.print(MORTE + " ");
				}
			}
			System.out.println();
		}
		System.out.println("Le nombre de cellule vivante est: "+count);
	}

	public static void initialisationPlateauJeu(){
		plateau = new int[size][size];
		for(int i = 0; i < size; i++){
			for(int j = 1; j < size; j++){
				plateau[i][j] = (int)(Math.random() * (VIVANT - MORTE + 1)) + MORTE;;
			}
		}
		System.out.println("\n".repeat(3));
	}

	public static int GestionVoisinsVivants(int[][] tab, int x, int y){
		int sum = 0;
		for(int i = -1; i < 2; i++){
			for(int j = -1; j < 2; j++){
				int col = (x + i + size)% size;
				int row = (y + j + size) % size;
				sum += tab[col][row];
			}
		}
		sum -= tab[x][y];
		return sum;
	}

	public static int[][] affecterUnTableau(int[][] originale, int[][] copie){
		for(int i = 0; i < originale.length; i++){
			for(int j = 0; j < originale[i].length; j++){
				copie[i][j] = originale[i][j];
			}
		}
		return copie;
	}

	public static int[][] GestionRegles(int[][] tableaunext, int jeu) {
		for(int i = 0; i < size; i++){
			for(int j = 0; j < size; j++){
				if(jeu == 1) tableaunext = gestionRegleJDLV(tableaunext, i, j);
				if(jeu == 2) tableaunext = gestionRegleDN(tableaunext, i, j);
			}
		}

		return tableaunext;
	}


	private static int[][] gestionRegleDN(int[][] tableaunext, int i, int j) {
		int contenu = plateau[i][j];
		int voisins = GestionVoisinsVivants(plateau, i, j);
		if((voisins == 3 || (voisins >= 6 && voisins <= 8 )) && contenu == 0){
			tableaunext[i][j] = 0;
		}else if(contenu == 1 && ((voisins >= 3 && voisins <= 4) || 
				(voisins >= 6 && voisins <= 8 ))){
			tableaunext[i][j] = 1;
		}else{
			tableaunext[i][j] = contenu;
		}
		return tableaunext;
	}

	public static int[][] gestionRegleJDLV(int[][] next, int i, int j) {
		int contenu = plateau[i][j];
		int voisins = GestionVoisinsVivants(plateau, i, j);
		if (voisins == 3 && contenu == 0) {
			next[i][j] = 1;
		} else if (contenu == 1 && (voisins < 2 || voisins > 3)) {
			next[i][j] = 0;
		} else {
			next[i][j] = contenu;
		}
		return next;
	}


	public static void GenerationSuivante(int jeu) {

		int[][] next = new int[size][size];
		AffichagePlateauJeu(plateau);
		System.out.println("Génération: "+ generation);
		while(true){
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			GestionRegles(next, jeu);
			if(generation == 8) {
				System.exit(0);
			}
			generation++;
			AffichagePlateauJeu(next);
			System.out.println("Génération: "+ generation);
			plateau = affecterUnTableau(next,plateau);
			System.out.println("\n".repeat(2));
		}
	}

}
