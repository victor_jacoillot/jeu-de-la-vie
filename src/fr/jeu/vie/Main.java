package fr.jeu.vie;

public class Main {
    
    public static void main(String[] args) {
    	    	
        System.out.println("Choisir un Jeu : \n"
        		+ "1 : Jeu de la Vie\n"
        		+ "2 : Day & Night\n"
        		+ "3 : QuadLife\n");
                
        int jeu = 2;
        
        ChoixJeu(jeu);
        
    }
    
    public static void ChoixJeu(int jeu) {
    	switch (jeu) {
		case 1:
			AutomateCellulaires.initialisationPlateauJeu();
			System.out.println("\n".repeat(2));
			AutomateCellulaires.GenerationSuivante(jeu);  
			break;

		case 2:
			AutomateCellulaires.initialisationPlateauJeu();
			System.out.println("\n".repeat(2));
			AutomateCellulaires.GenerationSuivante(jeu);  
			break;
			
		case 3:
			AutomateCellulaires.initialisationPlateauJeu();
			System.out.println("\n".repeat(2));
			AutomateCellulaires.GenerationSuivante(jeu);  
			break;
			
		default:
			System.out.println("Choix par defaut : Jeu de la vie ");
			AutomateCellulaires.initialisationPlateauJeu();
			System.out.println("\n".repeat(2));
			AutomateCellulaires.GenerationSuivante(1); 
			break;
		}
    }
}
